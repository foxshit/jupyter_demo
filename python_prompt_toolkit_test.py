
import sys
from prompt_toolkit import print_formatted_text, HTML

import pygments
from pygments.lexers.python import *
from pygments.token import Token
from prompt_toolkit.formatted_text import PygmentsTokens
from prompt_toolkit.styles import Style

print = print_formatted_text

def main():
    print(HTML("<red>this is test</red>"))
    file_name = sys.argv[0]

    print(file_name)
    f = open(file_name, "r")
    text= f.read()
    f.close()

    # Printing the output of a pygments lexer.
    tokens = list(pygments.lex(text, lexer = PythonLexer()))
    print_formatted_text(PygmentsTokens(tokens))


if __name__ == '__main__':
    main()
    